package dictionary

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/kylesferrazza/discordlib/cmd"
	"gitlab.com/kylesferrazza/discordlib/emoji/digits"
	"gitlab.com/kylesferrazza/discordlib/utils"
)

func defineCmd(sess *discordgo.Session, msg *discordgo.MessageCreate, args []string, fullArgs string) {
	for _, word := range args {
		result, err := dictLookup(word)
		if err != nil {
			utils.SendErrorMsg(sess, msg, "error fetching definition: "+err.Error())
			return
		}
		if len(result) < 1 {
			sess.MessageReactionAdd(msg.ChannelID, msg.ID, digits.Zero)
			utils.SendErrorMsg(sess, msg, "no results found for: "+word)
			return
		}
		reply := ""
		for _, res := range result {
			for _, meaning := range res.Meanings {
				for _, definition := range meaning.Definitions {
					reply += fmt.Sprintf("%s (%s): %s\n", res.Word, meaning.PartOfSpeech, definition.Definition)
				}
			}
		}
		utils.AckMsgCreate(sess, msg)
		sess.ChannelMessageSendReply(msg.ChannelID, reply, msg.MessageReference)
	}
}

var DefineCommandInfo = cmd.CommandInfo{
	Name:     "define",
	Handler:  defineCmd,
	ArgsText: "<word>",
	HelpText: "define the given word using the Merriam-Webster dictionary",
}
