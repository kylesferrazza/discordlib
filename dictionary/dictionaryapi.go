package dictionary

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"regexp"
)

type definition struct {
	Definition string    `json:"definition"`
	Example    string    `json:"example"`
	Synonyms   *[]string `json:"synonyms,omitempty"`
}

type meaning struct {
	PartOfSpeech string       `json:"partOfSpeech"`
	Definitions  []definition `json:"definitions"`
}

type phonetic struct {
	Text  string `json:"text"`
	Audio string `json:"audio"`
}

type dictResponse struct {
	Word      string     `json:"word"`
	Phonetics []phonetic `json:"phonetics"`
	Meanings  []meaning  `json:"meanings"`
}

type errResponse struct {
	Title string `json:"title"`
}

var alphabetic = regexp.MustCompile("[a-zA-Z]*")

func dictLookup(word string) ([]dictResponse, error) {
	if !alphabetic.MatchString(word) {
		return nil, errors.New("non-alphabetic word: " + word)
	}

	url := &url.URL{
		Scheme: "https",
		Host:   "api.dictionaryapi.dev",
		Path:   "api/v2/entries/en/" + url.PathEscape(word),
	}
	resp, err := http.Get(url.String())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	var result []dictResponse
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&result)
	if err != nil {
		return []dictResponse{}, nil
	}

	return result, nil
}
