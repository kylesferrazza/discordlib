package timer

import (
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

func formatDuration(duration time.Duration) string {
	return duration.Round(time.Second).String()
}

var TimerCommandName = "timer"

var TimerCommandDefinition = discordgo.ApplicationCommand{
	Name:        TimerCommandName,
	Description: "start a timer for the given number of seconds",
	Type:        discordgo.ChatApplicationCommand,
	Options: []*discordgo.ApplicationCommandOption{
		{
			Name:        "length",
			Type:        discordgo.ApplicationCommandOptionInteger,
			Required:    true,
			Description: "timer length in number of seconds",
		},
		{
			Name:        "name",
			Type:        discordgo.ApplicationCommandOptionString,
			Required:    false,
			Description: "name of the timer",
		},
	},
}

func TimerCommandHandler(s *discordgo.Session, i *discordgo.InteractionCreate) {
	lengthSet := false
	var length int64

	nameSet := false
	var name string

	for _, option := range i.ApplicationCommandData().Options {
		switch option.Name {
		case "length":
			length = option.IntValue()
			lengthSet = true
		case "name":
			name = option.StringValue()
			nameSet = true
		}
	}

	if !lengthSet {
		err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: "Timer length not given",
			},
		})
		if err != nil {
			logrus.WithError(err).WithField("cmd", TimerCommandName).Error("did not get length")
		}
		return
	}

	var responseContent string
	if nameSet {
		responseContent = fmt.Sprintf("%d-second timer [%s] started", length, name)
	} else {
		responseContent = fmt.Sprintf("%d-second timer started", length)
	}

	err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: responseContent,
		},
	})
	if err != nil {
		logrus.WithError(err).WithField("cmd", TimerCommandName).Error("Could not respond to command")
	}

	timerDuration := time.Second * time.Duration(length)
	endTime := time.Now().Add(timerDuration)
	var getRemainingText = func() string {
		timeUntilEnd := time.Until(endTime)
		if nameSet {
			return fmt.Sprintf("[%s]: %s remaining", name, formatDuration(timeUntilEnd))
		} else {
			return fmt.Sprintf("%s remaining", formatDuration(timeUntilEnd))
		}
	}
	msg, err := s.ChannelMessageSend(i.ChannelID, getRemainingText())
	if err != nil {
		logrus.WithError(err).Error("Could not send timer message")
		return
	}

	go func() {
		updateIncrement := 4
		for cur := 0; time.Now().Before(endTime); cur++ {
			if cur%updateIncrement == 0 {
				s.ChannelMessageEdit(msg.ChannelID, msg.ID, getRemainingText())
			}
			time.Sleep(time.Second / 2)
		}
		s.ChannelMessageDelete(msg.ChannelID, msg.ID)
		if nameSet {
			s.ChannelMessageSend(msg.ChannelID, fmt.Sprintf("Timer [%s] over!", name))
		} else {
			s.ChannelMessageSend(msg.ChannelID, "Timer over!")
		}
	}()
}
