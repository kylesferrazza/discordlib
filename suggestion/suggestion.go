package suggestion

import (
	"context"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"github.com/kobtea/go-todoist/todoist"
	"gitlab.com/kylesferrazza/discordlib/cmd"
	"gitlab.com/kylesferrazza/discordlib/utils"
)

var todoistClientInitialized bool
var todoistClient *todoist.Client
var todoistCtx context.Context

func setupClient(todoistKey string) error {
	if todoistClientInitialized {
		return nil
	}
	var err error
	todoistClient, err = todoist.NewClient("", todoistKey, "*", "", nil)
	if err != nil {
		return err
	}
	todoistCtx = context.Background()
	todoistClientInitialized = true
	return nil
}

func genSuggestionCmd(todoistKey string, projectID string, sectionID string) cmd.CommandHandler {
	err := setupClient(todoistKey)
	if err != nil {
		panic(fmt.Sprintf("error setting up todoist for suggestion command: %v", err))
	}
	return func(sess *discordgo.Session, msg *discordgo.MessageCreate, args []string, fullArgs string) {
		content := msg.Author.Username + ": " + fullArgs
		item, err := todoist.NewItem(content, &todoist.NewItemOpts{
			ProjectID: todoist.ID(projectID),
			SectionID: todoist.ID(sectionID),
		})
		if err != nil {
			utils.SendErrorMsg(sess, msg, "error contacting todoist")
			return
		}
		todoistClient.FullSync(todoistCtx, []todoist.Command{})
		todoistClient.Item.Add(*item)
		todoistClient.Commit(todoistCtx)
		utils.AckMsgCreate(sess, msg)
		sess.ChannelMessageSendReply(msg.ChannelID, "Thanks for the suggestion! Noted.", msg.MessageReference)
	}
}

func GenSuggestionCommandInfo(todoistKey string, projectID string, sectionID string) cmd.CommandInfo {
	return cmd.CommandInfo{
		Name:     "suggestion",
		Handler:  genSuggestionCmd(todoistKey, projectID, sectionID),
		HelpText: "<some suggestion for the bot here> - tell the bot you need it to do more",
	}
}
