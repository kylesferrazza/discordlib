package cmd

import (
	"fmt"
	"sort"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/kylesferrazza/discordlib/utils"
)

type CommandHandler func(sess *discordgo.Session, msg *discordgo.MessageCreate, args []string, fullArgs string)

type CommandInfo struct {
	Name     string
	Handler  CommandHandler
	ArgsText string
	HelpText string
	Hidden   bool
}

type CommandList []CommandInfo

func AddHelpCommandToMap(botName string, version string, cmdPrefix string, cmdList CommandList) {
	var helpHandler CommandHandler = func(sess *discordgo.Session, msg *discordgo.MessageCreate, args []string, fullArgs string) {
		utils.AckMsgCreate(sess, msg)

		var helpFields []*discordgo.MessageEmbedField

		sort.SliceStable(cmdList, func(i, j int) bool {
			return cmdList[i].Name < cmdList[j].Name
		})

		for _, cmdInfo := range cmdList {
			if cmdInfo.Hidden {
				continue
			}
			helpFields = append(helpFields, &discordgo.MessageEmbedField{
				Name:  cmdPrefix + cmdInfo.Name + " " + cmdInfo.ArgsText,
				Value: cmdInfo.HelpText,
			})
		}

		helpEmbed := discordgo.MessageEmbed{
			Title:       "help",
			Description: "help for " + botName + " " + version,
			Fields:      helpFields,
		}
		sess.ChannelMessageSendEmbed(msg.ChannelID, &helpEmbed)
	}
	cmdList = append(cmdList, CommandInfo{
		Name:     "help",
		Handler:  helpHandler,
		HelpText: "this help text",
	})
}

func (cmdList CommandList) findCmd(name string) (CommandInfo, bool) {
	for _, cmdInfo := range cmdList {
		if cmdInfo.Name == name {
			return cmdInfo, true
		}
	}
	return CommandInfo{}, false
}

func GenCmdHandler(sess *discordgo.Session, cmdPrefix string, cmdList CommandList) func(sess *discordgo.Session, msg *discordgo.MessageCreate) {
	return func(sess *discordgo.Session, msg *discordgo.MessageCreate) {
		if msg.Author.Bot {
			return
		}
		msgLen := len(msg.Content)
		if msgLen < 1 || !strings.HasPrefix(msg.Content, cmdPrefix) {
			return
		}

		afterPrefix := msg.Content[len(cmdPrefix):]
		fields := strings.Fields(afterPrefix)
		if len(fields) < 1 {
			utils.SendErrorMsg(sess, msg, "you did not specify a command!")
			return
		}
		afterFirstField := afterPrefix[len(fields[0]):]
		cmd := fields[0]
		if cmdInfo, present := cmdList.findCmd(cmd); present {
			cmdInfo.Handler(sess, msg, fields[1:], afterFirstField)
		} else {
			utils.SendErrorMsg(sess, msg, fmt.Sprintf("unknown command `%s`!", cmd))
		}
	}
}
