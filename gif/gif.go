package gif

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/kylesferrazza/discordlib/cmd"
	"gitlab.com/kylesferrazza/discordlib/utils"
)

type gifMedia struct {
	Url string `json:"url"`
}

type gifResultMap map[string]gifMedia

type gifResult struct {
	Url   string         `json:"url"`
	Media []gifResultMap `json:"media"`
}

type searchResult struct {
	Results []gifResult `json:"results"`
}

func searchTenor(tenorKey string, term string) (imgUrl string, e error) {
	v := url.Values{}
	v.Set("key", tenorKey)
	v.Set("q", term)
	v.Set("limit", "1")
	v.Set("contentfilter", "medium")
	url := &url.URL{
		Scheme:   "https",
		Host:     "api.tenor.com",
		Path:     "v1/search",
		RawQuery: v.Encode(),
	}
	resp, err := http.Get(url.String())
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	var result searchResult
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&result)
	if err != nil {
		return "", err
	}
	if len(result.Results) < 1 {
		return "", errors.New("no results found")
	}
	firstResult := result.Results[0]
	if len(firstResult.Media) < 1 {
		return "", errors.New("no media in first result")
	}
	firstMedia := firstResult.Media[0]
	gif, present := firstMedia["gif"]
	if !present {
		return "", errors.New("no GIF in first result")
	}
	return gif.Url, nil
}

func genGifCmd(tenorKey string) cmd.CommandHandler {
	return func(sess *discordgo.Session, msg *discordgo.MessageCreate, args []string, fullArgs string) {
		if len(args) < 1 {
			utils.SendErrorMsg(sess, msg, "no GIF search terms given")
		}

		gifUrl, err := searchTenor(tenorKey, fullArgs)
		if err != nil {
			utils.SendErrorMsg(sess, msg, fmt.Sprintf("error searching Tenor: %s", err))
			return
		}
		embedImage := discordgo.MessageEmbedImage{
			URL: gifUrl,
		}
		embed := discordgo.MessageEmbed{
			Color: 0x5ed31b,
			Image: &embedImage,
		}
		utils.AckMsgCreate(sess, msg)
		sess.ChannelMessageSendEmbed(msg.ChannelID, &embed)
	}
}

func GenGifCommandInfo(tenorKey string) cmd.CommandInfo {
	return cmd.CommandInfo{
		Name:     "gif",
		Handler:  genGifCmd(tenorKey),
		ArgsText: "<search term>",
		HelpText: "send a gif matching the given search term",
	}
}
