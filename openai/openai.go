package openai

import (
	"context"
	"fmt"

	"github.com/PullRequestInc/go-gpt3"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/kylesferrazza/discordlib/cmd"
	"gitlab.com/kylesferrazza/discordlib/utils"
)

func genOpenAiHandler(apiKey string) cmd.CommandHandler {
	return func(sess *discordgo.Session, msg *discordgo.MessageCreate, args []string, fullArgs string) {
		ctx := context.Background()
		client := gpt3.NewClient(apiKey, gpt3.WithDefaultEngine(gpt3.TextDavinci001Engine))
		fp := float32(1.0)

		resp, err := client.Completion(ctx, gpt3.CompletionRequest{
			Prompt:           []string{fullArgs},
			MaxTokens:        gpt3.IntPtr(512),
			Echo:             false,
			TopP:             &fp,
			FrequencyPenalty: 0.5,
		})
		if err != nil {
			utils.SendErrorMsg(sess, msg, fmt.Sprintf("Error contacting OpenAI: %v", err))
			return
		}

		utils.AckMsgCreate(sess, msg)
		sess.ChannelMessageSendReply(msg.ChannelID, resp.Choices[0].Text, msg.Reference())
	}
}

func GenOpenAiCommandInfo(apiKey string) cmd.CommandInfo {
	return cmd.CommandInfo{
		Name:     "complete",
		Handler:  genOpenAiHandler(apiKey),
		ArgsText: "...",
		HelpText: "ask OpenAI to complete your request",
	}
}
