module gitlab.com/kylesferrazza/discordlib

go 1.19

require (
	github.com/PullRequestInc/go-gpt3 v1.1.7
	github.com/bwmarrin/discordgo v0.26.1
	github.com/kobtea/go-todoist v0.2.2
)

require (
	github.com/fatih/color v1.13.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/satori/go.uuid v1.2.1-0.20180103174451-36e9d2ebbde5 // indirect
	github.com/sirupsen/logrus v1.9.0
	golang.org/x/crypto v0.0.0-20220829220503-c86fa9a7ed90 // indirect
	golang.org/x/sys v0.0.0-20220829200755-d48e67d00261 // indirect
)

replace github.com/kobtea/go-todoist => github.com/vrutkovs/go-todoist v0.2.3-0.20210529090621-3b6314df3c14
