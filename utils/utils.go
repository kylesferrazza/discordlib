package utils

import (
	"fmt"
	"os"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
	"gitlab.com/kylesferrazza/discordlib/emoji"
)

func SendErrorMsg(sess *discordgo.Session, msg *discordgo.MessageCreate, text string) {
	sess.MessageReactionAdd(msg.ChannelID, msg.ID, emoji.ThumbsDown)
	mention := msg.Author.Mention()
	replyMsg := fmt.Sprintf("%s: %s", mention, text)
	sess.ChannelMessageSend(msg.ChannelID, replyMsg)
}

func AckMsg(sess *discordgo.Session, msg *discordgo.MessageReference) {
	sess.MessageReactionAdd(msg.ChannelID, msg.MessageID, emoji.ThumbsUp)
}

func AckMsgCreate(sess *discordgo.Session, msg *discordgo.MessageCreate) {
	AckMsg(sess, msg.Reference())
}

func EnvOrPanic(name string) string {
	res, exists := os.LookupEnv(name)
	if !exists {
		logrus.WithField("var", name).Fatal("Required environment variable not set")
	}
	return res
}
