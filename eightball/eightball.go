package eightball

import (
	"math/rand"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

var responses = [...]string{
	"It is certain.",
	"It is decidedly so.",
	"Without a doubt.",
	"Yes - definitely.",
	"You may rely on it.",
	"As I see it, yes.",
	"Most likely.",
	"Outlook good.",
	"Yes.",
	"Signs point to yes.",
	"Reply hazy, try again.",
	"Ask again later.",
	"Better not tell you now.",
	"Cannot predict now.",
	"Concentrate and ask again.",
	"Don't count on it.",
	"My reply is no.",
	"My sources say no.",
	"Outlook not so good.",
	"Very doubtful.",
}

func eightball() string {
	index := rand.Intn(len(responses))
	return responses[index]
}

var EightBallCommandName = "8ball"

var EightBallCommandDefinition = discordgo.ApplicationCommand{
	Name:        EightBallCommandName,
	Description: "ask, and you shall receive",
	Type:        discordgo.ChatApplicationCommand,
	Options: []*discordgo.ApplicationCommandOption{
		{
			Name:        "question",
			Type:        discordgo.ApplicationCommandOptionString,
			Required:    true,
			Description: "your Y/N question",
		},
	},
}

func EightBallCommandHandler(s *discordgo.Session, i *discordgo.InteractionCreate) {
	err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: eightball(),
		},
	})
	if err != nil {
		logrus.WithError(err).WithField("cmd", EightBallCommandName).Error("Could not respond to command")
	}
}
