package emoji

const ThumbsUp = "👍"
const ThumbsDown = "👎"
const Heart = "❤️"
const Astonished = "😲"
const Plus = "➕"
const PeopleHoldingHands = "🧑‍🤝‍🧑"
const MiniDisc = "💽"
