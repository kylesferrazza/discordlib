package digits

const Zero = "0️⃣"
const One = "1️⃣"
const Two = "2️⃣"
const Three = "3️⃣"
const Four = "4️⃣"
const Five = "5️⃣"
const Six = "6️⃣"
const Seven = "7️⃣"
const Eight = "8️⃣"
const Nine = "9️⃣"
