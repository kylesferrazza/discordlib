package polls

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
	"gitlab.com/kylesferrazza/discordlib/emoji/alphabet"
)

var emojiLst []string = []string{
	alphabet.A,
	alphabet.B,
	alphabet.C,
	alphabet.D,
	alphabet.E,
	alphabet.F,
	alphabet.G,
	alphabet.H,
	alphabet.I,
	alphabet.J,
	alphabet.K,
	alphabet.L,
	alphabet.M,
	alphabet.N,
	alphabet.O,
	alphabet.P,
	alphabet.Q,
	alphabet.R,
	alphabet.S,
	alphabet.T,
	alphabet.U,
	alphabet.V,
	alphabet.W,
	alphabet.X,
	alphabet.Y,
	alphabet.Z,
}

func genPollEmbed(title string, userMention string, choices []string) discordgo.MessageEmbed {
	var fieldList []*discordgo.MessageEmbedField
	for i, option := range choices {
		if len(option) < 1 {
			continue
		}
		fieldList = append(fieldList, &discordgo.MessageEmbedField{
			Name:   emojiLst[i],
			Value:  option,
			Inline: true,
		})
	}
	return discordgo.MessageEmbed{
		Title:       "Poll: " + title,
		Description: "created by " + userMention,
		Fields:      fieldList,
	}
}

var PollsCommandName = "polls"

var PollsCommandDefinition = discordgo.ApplicationCommand{
	Name:        PollsCommandName,
	Description: "set up a poll",
	Type:        discordgo.ChatApplicationCommand,
	Options: []*discordgo.ApplicationCommandOption{
		{
			Name:        "title",
			Type:        discordgo.ApplicationCommandOptionString,
			Required:    true,
			Description: "title of the poll",
		},
		{
			Name:        "choices",
			Type:        discordgo.ApplicationCommandOptionString,
			Required:    true,
			Description: "poll || choices || separated || by || double || bars",
		},
	},
}

func PollsCommandHandler(s *discordgo.Session, i *discordgo.InteractionCreate) {
	choicesSet := false
	var choicesJoined string

	titleSet := false
	var title string

	for _, option := range i.ApplicationCommandData().Options {
		switch option.Name {
		case "choices":
			choicesJoined = option.StringValue()
			choicesSet = true
		case "title":
			title = option.StringValue()
			titleSet = true
		}
	}

	var respondError = func(errMsg string) {
		err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: errMsg,
			},
		})
		if err != nil {
			logrus.WithError(err).WithFields(
				logrus.Fields{
					"cmd":    PollsCommandName,
					"errMsg": errMsg,
				}).Error("could not send error response")
		}
	}

	if !choicesSet {
		respondError("Did not get choices for poll")
		return
	}

	if !titleSet {
		respondError("Did not get title for poll")
		return
	}

	choices := strings.Split(choicesJoined, "||")
	if len(choices) < 1 {
		respondError("Polls need at least one choice")
		return
	}

	numPossible := len(emojiLst)
	if len(choices) > numPossible {
		respondError(fmt.Sprintf("Polls can have at most %d choices", numPossible))
		return
	}

	var mention string
	if i.Interaction.Member != nil {
		mention = i.Interaction.Member.Mention()
	} else {
		mention = i.Interaction.User.Mention()
	}

	embed := genPollEmbed(title, mention, choices)

	err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Embeds: []*discordgo.MessageEmbed{
				&embed,
			},
		},
	})
	if err != nil {
		logrus.WithError(err).WithField("cmd", PollsCommandName).Error("Could not respond to command")
	}
	msg, err := s.InteractionResponse(i.Interaction)
	if err != nil {
		logrus.WithError(err).WithField("cmd", PollsCommandName).Error("Could not react to polls embed")
	}
	for choice := 0; choice < len(choices); choice++ {
		s.MessageReactionAdd(msg.ChannelID, msg.ID, emojiLst[choice])
	}
}
