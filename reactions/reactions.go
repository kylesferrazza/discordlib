package reactions

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
)

// Handles reacting to messages with the appropriate response/emoji based on message contents.
func GenMessageReactionHandler(
	emojiReactMap map[string]string,
	textReactMap map[string]string,
	imageReactMap map[string]string,
) func(sess *discordgo.Session, msg *discordgo.MessageCreate) {
	return func(sess *discordgo.Session, msg *discordgo.MessageCreate) {
		if msg.Author.Bot {
			return
		}
		fields := strings.Fields(msg.Content)
		for _, field := range fields {
			emoji, emojiPresent := emojiReactMap[field]
			if emojiPresent {
				fmt.Printf("Detected message containing [%s], reacting with [%s]\n", field, emoji)
				sess.MessageReactionAdd(msg.ChannelID, msg.ID, emoji)
			}

			text, textPresent := textReactMap[field]
			if textPresent {
				fmt.Printf("Detected message containing [%s], responding with [%s]\n", field, text)
				mention := msg.Author.Mention()
				msgText := fmt.Sprintf("%s: %s", mention, text)
				sess.ChannelMessageSend(msg.ChannelID, msgText)
			}

			imageUrl, imageTxtPresent := imageReactMap[field]
			if imageTxtPresent {
				fmt.Printf("Detected message containing [%s], responding with image [%s]\n", field, imageUrl)
				embedImage := discordgo.MessageEmbedImage{
					URL: imageUrl,
				}
				embed := discordgo.MessageEmbed{
					Image:       &embedImage,
					Title:       field,
					Description: msg.Author.Mention(),
				}
				sess.ChannelMessageSendEmbed(msg.ChannelID, &embed)
			}
		}
	}
}
