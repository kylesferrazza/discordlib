{
  description = "discordlib";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/master";

  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      name = "discordlib";
      buildInputs = with pkgs; [
        go_1_19
        gopls
      ];
    };
  };
}
